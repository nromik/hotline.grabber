﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Hotline.Grabber
{
  

    public partial class PriceModel
    {
        //[JsonProperty("sorting")]
        //public WelcomeSorting Sorting { get; set; }

        //[JsonProperty("filters")]
        //public WelcomeFilters Filters { get; set; }

        //[JsonProperty("specials")]
        //public Specials Specials { get; set; }

        [JsonProperty("prices")]
        public Price[] Prices { get; set; }
    }

    public partial class WelcomeFilters
    {
        [JsonProperty("counts")]
        public Dictionary<string, long> Counts { get; set; }

        [JsonProperty("available")]
        public The10041647550S27330Class Available { get; set; }

        [JsonProperty("price_filters")]
        public PriceFilters PriceFilters { get; set; }
    }

    public partial class The10041647550S27330Class
    {
        [JsonProperty("sales", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sales { get; set; }

        [JsonProperty("same_city")]
        public long SameCity { get; set; }

        [JsonProperty("same_region")]
        public long SameRegion { get; set; }

        [JsonProperty("official")]
        public long Official { get; set; }

        [JsonProperty("free_delivery", NullValueHandling = NullValueHandling.Ignore)]
        public long? FreeDelivery { get; set; }

        [JsonProperty("official_icon_show")]
        public long OfficialIconShow { get; set; }

        [JsonProperty("cond_new")]
        public long CondNew { get; set; }

        [JsonProperty("stock_available")]
        public long StockAvailable { get; set; }
    }

    public partial class PriceFilters
    {
        [JsonProperty("9308839440s1719")]
        public The10041647550S27330Class The9308839440S1719 { get; set; }

        [JsonProperty("9045510113s862")]
        public The10041647550S27330Class The9045510113S862 { get; set; }

        [JsonProperty("9468643867s27173")]
        public TartuGecko The9468643867S27173 { get; set; }

        [JsonProperty("9000098500s27173")]
        public TartuGecko The9000098500S27173 { get; set; }

        [JsonProperty("9514714715s23706")]
        public TartuGecko The9514714715S23706 { get; set; }

        [JsonProperty("9514714716s23706")]
        public TartuGecko The9514714716S23706 { get; set; }

        [JsonProperty("10056908761s23706")]
        public TartuGecko The10056908761S23706 { get; set; }

        [JsonProperty("9514714714s23706")]
        public TartuGecko The9514714714S23706 { get; set; }

        [JsonProperty("10056908759s23706")]
        public TartuGecko The10056908759S23706 { get; set; }

        [JsonProperty("10056908760s23706")]
        public TartuGecko The10056908760S23706 { get; set; }

        [JsonProperty("9561579960s14010")]
        public TartuGecko The9561579960S14010 { get; set; }

        [JsonProperty("9490383279s14010")]
        public TartuGecko The9490383279S14010 { get; set; }

        [JsonProperty("9505154486s16758")]
        public TartuGecko The9505154486S16758 { get; set; }

        [JsonProperty("9984490246s780")]
        public TartuGecko The9984490246S780 { get; set; }

        [JsonProperty("10032037856s20382")]
        public The10032037856_S20382 The10032037856S20382 { get; set; }

        [JsonProperty("9979687271s1111")]
        public TartuGecko The9979687271S1111 { get; set; }

        [JsonProperty("9979687266s1111")]
        public TartuGecko The9979687266S1111 { get; set; }

        [JsonProperty("9981829713s8549")]
        public TartuGecko The9981829713S8549 { get; set; }

        [JsonProperty("9981829710s8549")]
        public TartuGecko The9981829710S8549 { get; set; }

        [JsonProperty("10062981030s30390")]
        public TartuGecko The10062981030S30390 { get; set; }

        [JsonProperty("10056744135s287")]
        public TartuGecko The10056744135S287 { get; set; }

        [JsonProperty("10024509654s287")]
        public TartuGecko The10024509654S287 { get; set; }

        [JsonProperty("10070086213s26408")]
        public TartuGecko The10070086213S26408 { get; set; }

        [JsonProperty("9467704101s26408")]
        public TartuGecko The9467704101S26408 { get; set; }

        [JsonProperty("10026982396s17756")]
        public TartuGecko The10026982396S17756 { get; set; }

        [JsonProperty("10058459929s20293")]
        public TartuGecko The10058459929S20293 { get; set; }

        [JsonProperty("10028150857s21146")]
        public TartuGecko The10028150857S21146 { get; set; }

        [JsonProperty("10024715100s363")]
        public TartuGecko The10024715100S363 { get; set; }

        [JsonProperty("9781101257s22371")]
        public The10068300172_S22510 The9781101257S22371 { get; set; }

        [JsonProperty("9474905358s22371")]
        public The10068300172_S22510 The9474905358S22371 { get; set; }

        [JsonProperty("8712162617s31144")]
        public The10068300172_S22510 The8712162617S31144 { get; set; }

        [JsonProperty("10067431113s30813")]
        public TartuGecko The10067431113S30813 { get; set; }

        [JsonProperty("10035371839s30813")]
        public TartuGecko The10035371839S30813 { get; set; }

        [JsonProperty("9699588557s29359")]
        public TartuGecko The9699588557S29359 { get; set; }

        [JsonProperty("10070493077s26464")]
        public TartuGecko The10070493077S26464 { get; set; }

        [JsonProperty("9727938015s21871")]
        public TartuGecko The9727938015S21871 { get; set; }

        [JsonProperty("9727938023s21871")]
        public TartuGecko The9727938023S21871 { get; set; }

        [JsonProperty("9790502614s30553")]
        public TartuGecko The9790502614S30553 { get; set; }

        [JsonProperty("9790502266s30553")]
        public TartuGecko The9790502266S30553 { get; set; }

        [JsonProperty("10061979866s8171")]
        public TartuGecko The10061979866S8171 { get; set; }

        [JsonProperty("10070056240s3079")]
        public TartuGecko The10070056240S3079 { get; set; }

        [JsonProperty("10070056659s3079")]
        public TartuGecko The10070056659S3079 { get; set; }

        [JsonProperty("9787217556s33732")]
        public The10068300172_S22510 The9787217556S33732 { get; set; }

        [JsonProperty("10037009585s33732")]
        public The10032037856_S20382 The10037009585S33732 { get; set; }

        [JsonProperty("10070578437s14322")]
        public TartuGecko The10070578437S14322 { get; set; }

        [JsonProperty("9604127277s24770")]
        public The10068300172_S22510 The9604127277S24770 { get; set; }

        [JsonProperty("9843175345s23886")]
        public TartuGecko The9843175345S23886 { get; set; }

        [JsonProperty("9938548425s33503")]
        public The10068300172_S22510 The9938548425S33503 { get; set; }

        [JsonProperty("9938548422s33503")]
        public The10068300172_S22510 The9938548422S33503 { get; set; }

        [JsonProperty("10066792064s417")]
        public The10032037856_S20382 The10066792064S417 { get; set; }

        [JsonProperty("9625107267s32909")]
        public TartuGecko The9625107267S32909 { get; set; }

        [JsonProperty("9924378831s24901")]
        public The9918707921_S33093 The9924378831S24901 { get; set; }

        [JsonProperty("9615404274s22109")]
        public TartuGecko The9615404274S22109 { get; set; }

        [JsonProperty("10041647550s27330")]
        public The10041647550S27330Class The10041647550S27330 { get; set; }

        [JsonProperty("9102677014s25270")]
        public The10032037856_S20382 The9102677014S25270 { get; set; }

        [JsonProperty("9878786105s33675")]
        public The10068300172_S22510 The9878786105S33675 { get; set; }

        [JsonProperty("10068300172s22510")]
        public The10068300172_S22510 The10068300172S22510 { get; set; }

        [JsonProperty("9556261657s30302")]
        public The10068300172_S22510 The9556261657S30302 { get; set; }

        [JsonProperty("8872384516s30558")]
        public The10068300172_S22510 The8872384516S30558 { get; set; }

        [JsonProperty("9918707921s33093")]
        public The9918707921_S33093 The9918707921S33093 { get; set; }

        [JsonProperty("9932294957s24870")]
        public TartuGecko The9932294957S24870 { get; set; }

        [JsonProperty("9978988244s28975")]
        public TartuGecko The9978988244S28975 { get; set; }
    }

    public partial class TartuGecko
    {
        [JsonProperty("same_city")]
        public long SameCity { get; set; }

        [JsonProperty("same_region")]
        public long SameRegion { get; set; }

        [JsonProperty("cond_new")]
        public long CondNew { get; set; }

        [JsonProperty("stock_available")]
        public long StockAvailable { get; set; }

        [JsonProperty("free_delivery", NullValueHandling = NullValueHandling.Ignore)]
        public long? FreeDelivery { get; set; }

        [JsonProperty("sales", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sales { get; set; }
    }

    public partial class The10032037856_S20382
    {
        [JsonProperty("cond_new")]
        public long CondNew { get; set; }
    }

    public partial class The10068300172_S22510
    {
        [JsonProperty("cond_new")]
        public long CondNew { get; set; }

        [JsonProperty("stock_available")]
        public long StockAvailable { get; set; }
    }

    public partial class The9918707921_S33093
    {
        [JsonProperty("free_delivery")]
        public long FreeDelivery { get; set; }

        [JsonProperty("cond_new")]
        public long CondNew { get; set; }

        [JsonProperty("stock_available")]
        public long StockAvailable { get; set; }
    }

    public partial class Price
    {
        [JsonProperty("is_shop_new")]
        public long IsShopNew { get; set; }

        [JsonProperty("checkout_only")]
        public long CheckoutOnly { get; set; }

        [JsonProperty("new_shop_from_date")]
        public string NewShopFromDate { get; set; }

        //[JsonProperty("condition")]
        //public Condition Condition { get; set; }

        [JsonProperty("condition_title")]
        public string ConditionTitle { get; set; }

        [JsonProperty("rank")]
        public string Rank { get; set; }

        [JsonProperty("dt")]
        public DateTimeOffset Dt { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("popularity")]
        public long Popularity { get; set; }

        [JsonProperty("history_id")]
        public long HistoryId { get; set; }

        [JsonProperty("product_condition")]
        public long ProductCondition { get; set; }

        [JsonProperty("firm_city_id")]
        public long FirmCityId { get; set; }

        [JsonProperty("firm_city_genitive")]
        public string FirmCityGenitive { get; set; }

        [JsonProperty("firm_region_id")]
        public long FirmRegionId { get; set; }

        //[JsonProperty("currency")]
        //public Currency Currency { get; set; }

        [JsonProperty("official_vendor_status")]
        public long OfficialVendorStatus { get; set; }

        [JsonProperty("official_icon_show")]
        public long? OfficialIconShow { get; set; }

        [JsonProperty("official_vendor_status_hint")]
        public string OfficialVendorStatusHint { get; set; }

        [JsonProperty("official_vendor_status_link")]
        public string OfficialVendorStatusLink { get; set; }

        //[JsonProperty("merchant_title")]
        //public MerchantTitle MerchantTitle { get; set; }

        [JsonProperty("special")]
        public long Special { get; set; }

        [JsonProperty("is_official")]
        public bool IsOfficial { get; set; }

        [JsonProperty("bolder")]
        public long Bolder { get; set; }

        [JsonProperty("go_link")]
        public string GoLink { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("favorite_shop")]
        public long FavoriteShop { get; set; }

        [JsonProperty("firm_logo")]
        public string FirmLogo { get; set; }

        [JsonProperty("slogan")]
        public string Slogan { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("in_checkout")]
        public long? InCheckout { get; set; }

        [JsonProperty("price_uah")]
        public long PriceUah { get; set; }

        [JsonProperty("price_usd")]
        public long PriceUsd { get; set; }

        //[JsonProperty("price_old")]
        //public PriceOld PriceOld { get; set; }

        [JsonProperty("price_uah_real")]
        public string PriceUahReal { get; set; }

        [JsonProperty("price_uah_real_raw")]
        public double PriceUahRealRaw { get; set; }

        [JsonProperty("yp_id")]
        public long YpId { get; set; }

        [JsonProperty("firm_id")]
        public long FirmId { get; set; }

        [JsonProperty("firm_title")]
        public string FirmTitle { get; set; }

        [JsonProperty("firm_phones")]
        public string[] FirmPhones { get; set; }

        [JsonProperty("complaint_firm_title")]
        public string ComplaintFirmTitle { get; set; }

        [JsonProperty("complaint_title")]
        public string ComplaintTitle { get; set; }

        [JsonProperty("firm_webshop")]
        public string FirmWebshop { get; set; }

        [JsonProperty("firm_website")]
        public string FirmWebsite { get; set; }

        //[JsonProperty("shopRating")]
        //public ShopRating ShopRating { get; set; }

        //[JsonProperty("delivery_settings")]
        //public DeliverySettings DeliverySettings { get; set; }

        //[JsonProperty("firm_shop_points")]
        //public FirmShopPointsUnion FirmShopPoints { get; set; }

        //[JsonProperty("guarantee")]
        //public Guarantee Guarantee { get; set; }

        [JsonProperty("order_days")]
        public long OrderDays { get; set; }

        //[JsonProperty("stock_type")]
        //public StockType StockType { get; set; }

        [JsonProperty("stock_type_id")]
        public long StockTypeId { get; set; }

        [JsonProperty("guarantee_format")]
        public string GuaranteeFormat { get; set; }

        [JsonProperty("has_sales")]
        public bool HasSales { get; set; }

        [JsonProperty("has_free_delivery")]
        public bool HasFreeDelivery { get; set; }

        [JsonProperty("raw_offer_title")]
        public string RawOfferTitle { get; set; }

        [JsonProperty("titleShort")]
        public string TitleShort { get; set; }

        //[JsonProperty("price_line_type")]
        //public PriceLineType PriceLineType { get; set; }

        [JsonProperty("price_decimals")]
        public string PriceDecimals { get; set; }

        [JsonProperty("dollar_price_disabled")]
        public bool DollarPriceDisabled { get; set; }

        //[JsonProperty("price_usd_real")]
        //public PriceUsdRealUnion PriceUsdReal { get; set; }

        //[JsonProperty("price_old_decimals")]
        //public PriceOld PriceOldDecimals { get; set; }

        [JsonProperty("is_auction")]
        public bool IsAuction { get; set; }

        [JsonProperty("cardid")]
        public long Cardid { get; set; }

        //[JsonProperty("sale")]
        //public SaleUnion Sale { get; set; }

        [JsonProperty("hide_guarantee")]
        public bool HideGuarantee { get; set; }

        //[JsonProperty("delivery")]
        //public Delivery Delivery { get; set; }
    }

    public partial class Delivery
    {
        [JsonProperty("has_free_delivery")]
        public bool HasFreeDelivery { get; set; }

        [JsonProperty("methods")]
        public MethodsUnion Methods { get; set; }

        [JsonProperty("origin")]
        public bool Origin { get; set; }

        [JsonProperty("same_city")]
        public bool SameCity { get; set; }

        [JsonProperty("another_city")]
        public bool AnotherCity { get; set; }

        [JsonProperty("same_region")]
        public bool SameRegion { get; set; }

        [JsonProperty("same_region_city")]
        public SameRegionCityUnion SameRegionCity { get; set; }

        [JsonProperty("edit_link")]
        public bool EditLink { get; set; }
    }

    public partial class MethodsClass
    {
        [JsonProperty("pickup", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Address> Pickup { get; set; }

        [JsonProperty("warehouse", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Address> Warehouse { get; set; }

        [JsonProperty("address", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Address> Address { get; set; }
    }

    public partial class Address
    {
        [JsonProperty("delivery_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long DeliveryId { get; set; }

        [JsonProperty("cost")]
        public long? Cost { get; set; }

        [JsonProperty("price_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? PriceId { get; set; }

        [JsonProperty("free_from")]
        public long? FreeFrom { get; set; }

        [JsonProperty("time")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Time { get; set; }

        [JsonProperty("type")]
        public AddressType Type { get; set; }

        [JsonProperty("firm_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long FirmId { get; set; }

        [JsonProperty("carrier")]
        public Carrier? Carrier { get; set; }

        [JsonProperty("delivery_name")]
        public DeliveryName DeliveryName { get; set; }

        [JsonProperty("in_checkout")]
        public InCheckout InCheckout { get; set; }

        [JsonProperty("available_for_checkout")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long AvailableForCheckout { get; set; }

        [JsonProperty("delivery_origin")]
        public DeliveryOrigin DeliveryOrigin { get; set; }

        [JsonProperty("warehouse_title")]
        public WarehouseTitle? WarehouseTitle { get; set; }

        [JsonProperty("service_name_ablative")]
        public ServiceNameAblative? ServiceNameAblative { get; set; }

        [JsonProperty("service_name_genitive")]
        public ServiceNameGenitive? ServiceNameGenitive { get; set; }

        [JsonProperty("service_name")]
        public ServiceName? ServiceName { get; set; }

        [JsonProperty("delivery_times")]
        public DeliveryTimes DeliveryTimes { get; set; }

        [JsonProperty("cost_explanation")]
        public CostExplanation CostExplanation { get; set; }
    }

    public partial class DeliverySetting
    {
        [JsonProperty("cost", NullValueHandling = NullValueHandling.Ignore)]
        public long? Cost { get; set; }

        [JsonProperty("free_from", NullValueHandling = NullValueHandling.Ignore)]
        public long? FreeFrom { get; set; }
    }

    public partial class FirmShopPointsClass
    {
        [JsonProperty("0", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The0 { get; set; }

        [JsonProperty("1", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The1 { get; set; }

        [JsonProperty("32", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The32 { get; set; }

        [JsonProperty("33", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The33 { get; set; }

        [JsonProperty("35", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The35 { get; set; }

        [JsonProperty("49", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The49 { get; set; }

        [JsonProperty("67", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The67 { get; set; }

        [JsonProperty("68", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The68 { get; set; }

        [JsonProperty("69", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The69 { get; set; }

        [JsonProperty("71", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The71 { get; set; }

        [JsonProperty("89", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The89 { get; set; }

        [JsonProperty("90", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The90 { get; set; }

        [JsonProperty("117", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The117 { get; set; }

        [JsonProperty("134", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The134 { get; set; }

        [JsonProperty("135", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The135 { get; set; }

        [JsonProperty("154", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The154 { get; set; }

        [JsonProperty("155", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The155 { get; set; }

        [JsonProperty("173", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The173 { get; set; }

        [JsonProperty("175", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The175 { get; set; }

        [JsonProperty("187", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The187 { get; set; }

        [JsonProperty("188", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The188 { get; set; }

        [JsonProperty("205", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The205 { get; set; }

        [JsonProperty("223", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The223 { get; set; }

        [JsonProperty("224", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The224 { get; set; }

        [JsonProperty("226", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The226 { get; set; }

        [JsonProperty("245", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The245 { get; set; }

        [JsonProperty("264", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The264 { get; set; }

        [JsonProperty("266", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The266 { get; set; }

        [JsonProperty("282", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The282 { get; set; }

        [JsonProperty("300", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The300 { get; set; }

        [JsonProperty("313", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The313 { get; set; }

        [JsonProperty("356", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The356 { get; set; }

        [JsonProperty("361", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The361 { get; set; }

        [JsonProperty("370", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The370 { get; set; }

        [JsonProperty("371", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The371 { get; set; }

        [JsonProperty("372", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The372 { get; set; }

        [JsonProperty("373", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The373 { get; set; }

        [JsonProperty("374", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The374 { get; set; }

        [JsonProperty("394", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The394 { get; set; }

        [JsonProperty("395", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The395 { get; set; }

        [JsonProperty("396", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The396 { get; set; }

        [JsonProperty("401", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The401 { get; set; }

        [JsonProperty("403", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The403 { get; set; }

        [JsonProperty("404", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The404 { get; set; }

        [JsonProperty("407", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The407 { get; set; }

        [JsonProperty("411", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The411 { get; set; }

        [JsonProperty("414", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The414 { get; set; }

        [JsonProperty("415", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The415 { get; set; }

        [JsonProperty("416", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The416 { get; set; }

        [JsonProperty("429", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The429 { get; set; }

        [JsonProperty("442", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The442 { get; set; }

        [JsonProperty("443", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The443 { get; set; }

        [JsonProperty("458", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The458 { get; set; }

        [JsonProperty("459", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The459 { get; set; }

        [JsonProperty("461", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The461 { get; set; }

        [JsonProperty("462", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The462 { get; set; }

        [JsonProperty("490", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The490 { get; set; }

        [JsonProperty("491", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The491 { get; set; }

        [JsonProperty("", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> Empty { get; set; }

        [JsonProperty("point_ids")]
        public PointId[] PointIds { get; set; }

        [JsonProperty("regions")]
        public Regions Regions { get; set; }

        [JsonProperty("hl_regions")]
        public Regions HlRegions { get; set; }

        [JsonProperty("72", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The72 { get; set; }

        [JsonProperty("73", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The73 { get; set; }

        [JsonProperty("74", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The74 { get; set; }

        [JsonProperty("76", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The76 { get; set; }

        [JsonProperty("550", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The550 { get; set; }

        [JsonProperty("5064", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5064 { get; set; }

        [JsonProperty("2", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The2 { get; set; }

        [JsonProperty("156", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The156 { get; set; }

        [JsonProperty("160", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The160 { get; set; }

        [JsonProperty("895", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The895 { get; set; }

        [JsonProperty("5982", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5982 { get; set; }

        [JsonProperty("5988", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5988 { get; set; }

        [JsonProperty("5989", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5989 { get; set; }

        [JsonProperty("5997", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5997 { get; set; }

        [JsonProperty("6003", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The6003 { get; set; }

        [JsonProperty("4", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The4 { get; set; }

        [JsonProperty("5", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5 { get; set; }

        [JsonProperty("6", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The6 { get; set; }

        [JsonProperty("283", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The283 { get; set; }

        [JsonProperty("5756", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5756 { get; set; }

        [JsonProperty("5804", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5804 { get; set; }

        [JsonProperty("265", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The265 { get; set; }

        [JsonProperty("306", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The306 { get; set; }

        [JsonProperty("5434", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5434 { get; set; }

        [JsonProperty("5852", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The5852 { get; set; }

        [JsonProperty("6012", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The6012 { get; set; }

        [JsonProperty("430", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The430 { get; set; }

        [JsonProperty("432", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> The432 { get; set; }
    }

    public partial class Regions
    {
        [JsonProperty("1", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The1 { get; set; }

        [JsonProperty("2", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The2 { get; set; }

        [JsonProperty("3", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The3 { get; set; }

        [JsonProperty("4", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The4 { get; set; }

        [JsonProperty("5", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The5 { get; set; }

        [JsonProperty("6", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The6 { get; set; }

        [JsonProperty("7", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The7 { get; set; }

        [JsonProperty("8", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The8 { get; set; }

        [JsonProperty("9", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The9 { get; set; }

        [JsonProperty("10", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The10 { get; set; }

        [JsonProperty("11", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The11 { get; set; }

        [JsonProperty("12", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The12 { get; set; }

        [JsonProperty("13", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The13 { get; set; }

        [JsonProperty("14", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The14 { get; set; }

        [JsonProperty("15", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The15 { get; set; }

        [JsonProperty("16", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The16 { get; set; }

        [JsonProperty("17", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The17 { get; set; }

        [JsonProperty("18", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The18 { get; set; }

        [JsonProperty("19", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The19 { get; set; }

        [JsonProperty("20", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The20 { get; set; }

        [JsonProperty("21", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The21 { get; set; }

        [JsonProperty("22", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The22 { get; set; }

        [JsonProperty("23", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The23 { get; set; }

        [JsonProperty("24", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The24 { get; set; }

        [JsonProperty("25", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The25 { get; set; }

        [JsonProperty("")]
        public object Empty { get; set; }

        [JsonProperty("26", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The26 { get; set; }

        [JsonProperty("27", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The27 { get; set; }

        [JsonProperty("70", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? The70 { get; set; }
    }

    public partial class Guarantee
    {
        [JsonProperty("type")]
        public GuaranteeType? Type { get; set; }

        [JsonProperty("term")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Term { get; set; }

        [JsonProperty("sort")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Sort { get; set; }
    }

    public partial class SaleClass
    {
        [JsonProperty("firm_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long FirmId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("manual_created")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ManualCreated { get; set; }

        [JsonProperty("holiday_title")]
        public string HolidayTitle { get; set; }

        [JsonProperty("offer_id")]
        public string OfferId { get; set; }

        [JsonProperty("sales_strict_mode")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long SalesStrictMode { get; set; }

        [JsonProperty("strict_offer_id")]
        public object StrictOfferId { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public partial class ShopRating
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("yp_id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long YpId { get; set; }

        [JsonProperty("rating")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Rating { get; set; }

        [JsonProperty("reviews_quantity")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long ReviewsQuantity { get; set; }

        [JsonProperty("reviews_quantity_title")]
        public ReviewsQuantityTitle ReviewsQuantityTitle { get; set; }
    }

    public partial class WelcomeSorting
    {
        [JsonProperty("current")]
        public long Current { get; set; }

        [JsonProperty("available")]
        public PlaceClass Available { get; set; }

        [JsonProperty("places")]
        public Dictionary<string, PlaceClass> Places { get; set; }

        [JsonProperty("assign_places")]
        public string[] AssignPlaces { get; set; }
    }

    public partial class PlaceClass
    {
        [JsonProperty("price_asc")]
        public long PriceAsc { get; set; }

        [JsonProperty("price_desc")]
        public long PriceDesc { get; set; }

        [JsonProperty("guarantee")]
        public long Guarantee { get; set; }

        [JsonProperty("rating")]
        public long Rating { get; set; }

        [JsonProperty("bids_desc")]
        public long BidsDesc { get; set; }
    }

    public partial class Specials
    {
        [JsonProperty("default")]
        public long Default { get; set; }

        [JsonProperty("sorting")]
        public SpecialsSorting Sorting { get; set; }

        [JsonProperty("filters")]
        public SpecialsFilters Filters { get; set; }
    }

    public partial class SpecialsFilters
    {
        [JsonProperty("cond_used")]
        public long CondUsed { get; set; }
    }

    public partial class SpecialsSorting
    {
        [JsonProperty("price_asc")]
        public long PriceAsc { get; set; }

        [JsonProperty("bids_desc")]
        public long BidsDesc { get; set; }
    }

    public enum Condition { New };

    public enum Currency { Uah };

    public enum Carrier { Da, Me, Np, Slf, Up, Zd };

    public enum CostExplanation { The100Грн, The150Грн, The25Грн, The42Грн, The49Грн, The50Грн, The60Грн, The75Грн, The99Грн, Бесплатно, ПоТарифамПеревозчика };

    public enum DeliveryName { Empty, ВсяУкраина, ДоступноДля22Областей, ДоступноДля24Областей, ДоступноДляВсехГородовУкраины, ДоступноДляВсіхМістУкраїни, ДоступноДляДнепропетровскойКиевскойОбластей, ДоступноДляКиева, ДоступноДляКиевскойОбласти, ДоступноДляКиєва, КурьерскаяДоставкаПоКиеву };

    public enum DeliveryOrigin { Cabinet, Price };

    public enum DeliveryTimes { The024Часа, The1014Дней, The13Дня, The49Дней };

    public enum ServiceName { Empty, Деливери, ЗручнаДоставка, МистЭкспресс, НоваяПочта, Укрпочта };

    public enum ServiceNameAblative { Деливери, ЗручнаДоставка, Курьером, МистЭкспресс, НовойПочтой, Укрпочтой };

    public enum ServiceNameGenitive { Empty, Деливери, ЗручнаДоставка, МистЭкспресс, НовойПочты, Укрпочты };

    public enum AddressType { Address, Pickup, Warehouse };

    public enum WarehouseTitle { ДоСклада };

    public enum SameRegionCityEnum { Киева };

    public enum GuaranteeType { ОтМагазина, ОтПроизводителя };

    public enum MerchantTitle { Cherries, Dms, Empty };

    public enum PriceLineType { Tx };

    public enum PriceUsdRealEnum { The9Nbsp999Nbsp999 };

    public enum ReviewsQuantityTitle { Отзыв, Отзыва, Отзывов };

    public enum StockType { Empty, ВНаличии, ПодЗаказ };

    public partial struct InCheckout
    {
        public bool? Bool;
        public long? Integer;

        public static implicit operator InCheckout(bool Bool) => new InCheckout { Bool = Bool };
        public static implicit operator InCheckout(long Integer) => new InCheckout { Integer = Integer };
        public bool IsNull => Bool == null && Integer == null;
    }

    public partial struct MethodsUnion
    {
        public object[] AnythingArray;
        public MethodsClass MethodsClass;

        public static implicit operator MethodsUnion(object[] AnythingArray) => new MethodsUnion { AnythingArray = AnythingArray };
        public static implicit operator MethodsUnion(MethodsClass MethodsClass) => new MethodsUnion { MethodsClass = MethodsClass };
    }

    public partial struct SameRegionCityUnion
    {
        public bool? Bool;
        public SameRegionCityEnum? Enum;

        public static implicit operator SameRegionCityUnion(bool Bool) => new SameRegionCityUnion { Bool = Bool };
        public static implicit operator SameRegionCityUnion(SameRegionCityEnum Enum) => new SameRegionCityUnion { Enum = Enum };
    }

    public partial struct DeliverySettings
    {
        public object[] AnythingArray;
        public bool? Bool;
        public Dictionary<string, DeliverySetting> DeliverySettingMap;

        public static implicit operator DeliverySettings(object[] AnythingArray) => new DeliverySettings { AnythingArray = AnythingArray };
        public static implicit operator DeliverySettings(bool Bool) => new DeliverySettings { Bool = Bool };
        public static implicit operator DeliverySettings(Dictionary<string, DeliverySetting> DeliverySettingMap) => new DeliverySettings { DeliverySettingMap = DeliverySettingMap };
    }

    public partial struct PointId
    {
        public long? Integer;
        public string String;

        public static implicit operator PointId(long Integer) => new PointId { Integer = Integer };
        public static implicit operator PointId(string String) => new PointId { String = String };
    }

    public partial struct FirmShopPointsUnion
    {
        public bool? Bool;
        public FirmShopPointsClass FirmShopPointsClass;

        public static implicit operator FirmShopPointsUnion(bool Bool) => new FirmShopPointsUnion { Bool = Bool };
        public static implicit operator FirmShopPointsUnion(FirmShopPointsClass FirmShopPointsClass) => new FirmShopPointsUnion { FirmShopPointsClass = FirmShopPointsClass };
    }

    public partial struct PriceOld
    {
        public bool? Bool;
        public string String;

        public static implicit operator PriceOld(bool Bool) => new PriceOld { Bool = Bool };
        public static implicit operator PriceOld(string String) => new PriceOld { String = String };
    }

    public partial struct PriceUsdRealUnion
    {
        public PriceUsdRealEnum? Enum;
        public long? Integer;

        public static implicit operator PriceUsdRealUnion(PriceUsdRealEnum Enum) => new PriceUsdRealUnion { Enum = Enum };
        public static implicit operator PriceUsdRealUnion(long Integer) => new PriceUsdRealUnion { Integer = Integer };
    }

    public partial struct SaleUnion
    {
        public bool? Bool;
        public SaleClass SaleClass;

        public static implicit operator SaleUnion(bool Bool) => new SaleUnion { Bool = Bool };
        public static implicit operator SaleUnion(SaleClass SaleClass) => new SaleUnion { SaleClass = SaleClass };
    }

    

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                ConditionConverter.Singleton,
                CurrencyConverter.Singleton,
                MethodsUnionConverter.Singleton,
                CarrierConverter.Singleton,
                CostExplanationConverter.Singleton,
                DeliveryNameConverter.Singleton,
                DeliveryOriginConverter.Singleton,
                DeliveryTimesConverter.Singleton,
                InCheckoutConverter.Singleton,
                ServiceNameConverter.Singleton,
                ServiceNameAblativeConverter.Singleton,
                ServiceNameGenitiveConverter.Singleton,
                AddressTypeConverter.Singleton,
                WarehouseTitleConverter.Singleton,
                SameRegionCityUnionConverter.Singleton,
                SameRegionCityEnumConverter.Singleton,
                DeliverySettingsConverter.Singleton,
                FirmShopPointsUnionConverter.Singleton,
                PointIdConverter.Singleton,
                GuaranteeTypeConverter.Singleton,
                MerchantTitleConverter.Singleton,
                PriceLineTypeConverter.Singleton,
                PriceOldConverter.Singleton,
                PriceUsdRealUnionConverter.Singleton,
                PriceUsdRealEnumConverter.Singleton,
                SaleUnionConverter.Singleton,
                ReviewsQuantityTitleConverter.Singleton,
                StockTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }

    internal class ConditionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Condition) || t == typeof(Condition?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "new")
            {
                return Condition.New;
            }
            throw new Exception("Cannot unmarshal type Condition");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Condition)untypedValue;
            if (value == Condition.New)
            {
                serializer.Serialize(writer, "new");
                return;
            }
            throw new Exception("Cannot marshal type Condition");
        }

        public static readonly ConditionConverter Singleton = new ConditionConverter();
    }

    internal class CurrencyConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Currency) || t == typeof(Currency?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "uah")
            {
                return Currency.Uah;
            }
            throw new Exception("Cannot unmarshal type Currency");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Currency)untypedValue;
            if (value == Currency.Uah)
            {
                serializer.Serialize(writer, "uah");
                return;
            }
            throw new Exception("Cannot marshal type Currency");
        }

        public static readonly CurrencyConverter Singleton = new CurrencyConverter();
    }

    internal class MethodsUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(MethodsUnion) || t == typeof(MethodsUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.StartObject:
                    var objectValue = serializer.Deserialize<MethodsClass>(reader);
                    return new MethodsUnion { MethodsClass = objectValue };
                case JsonToken.StartArray:
                    var arrayValue = serializer.Deserialize<object[]>(reader);
                    return new MethodsUnion { AnythingArray = arrayValue };
            }
            throw new Exception("Cannot unmarshal type MethodsUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (MethodsUnion)untypedValue;
            if (value.AnythingArray != null)
            {
                serializer.Serialize(writer, value.AnythingArray);
                return;
            }
            if (value.MethodsClass != null)
            {
                serializer.Serialize(writer, value.MethodsClass);
                return;
            }
            throw new Exception("Cannot marshal type MethodsUnion");
        }

        public static readonly MethodsUnionConverter Singleton = new MethodsUnionConverter();
    }

    internal class CarrierConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Carrier) || t == typeof(Carrier?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "DA":
                    return Carrier.Da;
                case "ME":
                    return Carrier.Me;
                case "NP":
                    return Carrier.Np;
                case "SLF":
                    return Carrier.Slf;
                case "UP":
                    return Carrier.Up;
                case "ZD":
                    return Carrier.Zd;
            }
            throw new Exception("Cannot unmarshal type Carrier");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Carrier)untypedValue;
            switch (value)
            {
                case Carrier.Da:
                    serializer.Serialize(writer, "DA");
                    return;
                case Carrier.Me:
                    serializer.Serialize(writer, "ME");
                    return;
                case Carrier.Np:
                    serializer.Serialize(writer, "NP");
                    return;
                case Carrier.Slf:
                    serializer.Serialize(writer, "SLF");
                    return;
                case Carrier.Up:
                    serializer.Serialize(writer, "UP");
                    return;
                case Carrier.Zd:
                    serializer.Serialize(writer, "ZD");
                    return;
            }
            throw new Exception("Cannot marshal type Carrier");
        }

        public static readonly CarrierConverter Singleton = new CarrierConverter();
    }

    internal class CostExplanationConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(CostExplanation) || t == typeof(CostExplanation?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "100 грн":
                    return CostExplanation.The100Грн;
                case "150 грн":
                    return CostExplanation.The150Грн;
                case "25 грн":
                    return CostExplanation.The25Грн;
                case "42 грн":
                    return CostExplanation.The42Грн;
                case "49 грн":
                    return CostExplanation.The49Грн;
                case "50 грн":
                    return CostExplanation.The50Грн;
                case "60 грн":
                    return CostExplanation.The60Грн;
                case "75 грн":
                    return CostExplanation.The75Грн;
                case "99 грн":
                    return CostExplanation.The99Грн;
                case "бесплатно":
                    return CostExplanation.Бесплатно;
                case "по тарифам перевозчика":
                    return CostExplanation.ПоТарифамПеревозчика;
            }
            throw new Exception("Cannot unmarshal type CostExplanation");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (CostExplanation)untypedValue;
            switch (value)
            {
                case CostExplanation.The100Грн:
                    serializer.Serialize(writer, "100 грн");
                    return;
                case CostExplanation.The150Грн:
                    serializer.Serialize(writer, "150 грн");
                    return;
                case CostExplanation.The25Грн:
                    serializer.Serialize(writer, "25 грн");
                    return;
                case CostExplanation.The42Грн:
                    serializer.Serialize(writer, "42 грн");
                    return;
                case CostExplanation.The49Грн:
                    serializer.Serialize(writer, "49 грн");
                    return;
                case CostExplanation.The50Грн:
                    serializer.Serialize(writer, "50 грн");
                    return;
                case CostExplanation.The60Грн:
                    serializer.Serialize(writer, "60 грн");
                    return;
                case CostExplanation.The75Грн:
                    serializer.Serialize(writer, "75 грн");
                    return;
                case CostExplanation.The99Грн:
                    serializer.Serialize(writer, "99 грн");
                    return;
                case CostExplanation.Бесплатно:
                    serializer.Serialize(writer, "бесплатно");
                    return;
                case CostExplanation.ПоТарифамПеревозчика:
                    serializer.Serialize(writer, "по тарифам перевозчика");
                    return;
            }
            throw new Exception("Cannot marshal type CostExplanation");
        }

        public static readonly CostExplanationConverter Singleton = new CostExplanationConverter();
    }

    internal class DeliveryNameConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DeliveryName) || t == typeof(DeliveryName?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return DeliveryName.Empty;
                case "Доступно для 22 областей":
                    return DeliveryName.ДоступноДля22Областей;
                case "Доступно для 24 областей":
                    return DeliveryName.ДоступноДля24Областей;
                case "Доступно для Днепропетровской, Киевской областей":
                    return DeliveryName.ДоступноДляДнепропетровскойКиевскойОбластей;
                case "Доступно для Киева":
                    return DeliveryName.ДоступноДляКиева;
                case "Доступно для Киевской области":
                    return DeliveryName.ДоступноДляКиевскойОбласти;
                case "Доступно для Києва":
                    return DeliveryName.ДоступноДляКиєва;
                case "Доступно для всех городов Украины":
                    return DeliveryName.ДоступноДляВсехГородовУкраины;
                case "Доступно для всіх міст України":
                    return DeliveryName.ДоступноДляВсіхМістУкраїни;
                case "Курьерская доставка по Киеву":
                    return DeliveryName.КурьерскаяДоставкаПоКиеву;
                case "вся Украина":
                    return DeliveryName.ВсяУкраина;
            }
            throw new Exception("Cannot unmarshal type DeliveryName");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DeliveryName)untypedValue;
            switch (value)
            {
                case DeliveryName.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case DeliveryName.ДоступноДля22Областей:
                    serializer.Serialize(writer, "Доступно для 22 областей");
                    return;
                case DeliveryName.ДоступноДля24Областей:
                    serializer.Serialize(writer, "Доступно для 24 областей");
                    return;
                case DeliveryName.ДоступноДляДнепропетровскойКиевскойОбластей:
                    serializer.Serialize(writer, "Доступно для Днепропетровской, Киевской областей");
                    return;
                case DeliveryName.ДоступноДляКиева:
                    serializer.Serialize(writer, "Доступно для Киева");
                    return;
                case DeliveryName.ДоступноДляКиевскойОбласти:
                    serializer.Serialize(writer, "Доступно для Киевской области");
                    return;
                case DeliveryName.ДоступноДляКиєва:
                    serializer.Serialize(writer, "Доступно для Києва");
                    return;
                case DeliveryName.ДоступноДляВсехГородовУкраины:
                    serializer.Serialize(writer, "Доступно для всех городов Украины");
                    return;
                case DeliveryName.ДоступноДляВсіхМістУкраїни:
                    serializer.Serialize(writer, "Доступно для всіх міст України");
                    return;
                case DeliveryName.КурьерскаяДоставкаПоКиеву:
                    serializer.Serialize(writer, "Курьерская доставка по Киеву");
                    return;
                case DeliveryName.ВсяУкраина:
                    serializer.Serialize(writer, "вся Украина");
                    return;
            }
            throw new Exception("Cannot marshal type DeliveryName");
        }

        public static readonly DeliveryNameConverter Singleton = new DeliveryNameConverter();
    }

    internal class DeliveryOriginConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DeliveryOrigin) || t == typeof(DeliveryOrigin?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "cabinet":
                    return DeliveryOrigin.Cabinet;
                case "price":
                    return DeliveryOrigin.Price;
            }
            throw new Exception("Cannot unmarshal type DeliveryOrigin");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DeliveryOrigin)untypedValue;
            switch (value)
            {
                case DeliveryOrigin.Cabinet:
                    serializer.Serialize(writer, "cabinet");
                    return;
                case DeliveryOrigin.Price:
                    serializer.Serialize(writer, "price");
                    return;
            }
            throw new Exception("Cannot marshal type DeliveryOrigin");
        }

        public static readonly DeliveryOriginConverter Singleton = new DeliveryOriginConverter();
    }

    internal class DeliveryTimesConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DeliveryTimes) || t == typeof(DeliveryTimes?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "0-24 часа":
                    return DeliveryTimes.The024Часа;
                case "1-3 дня":
                    return DeliveryTimes.The13Дня;
                case "10-14 дней":
                    return DeliveryTimes.The1014Дней;
                case "4-9 дней":
                    return DeliveryTimes.The49Дней;
            }
            throw new Exception("Cannot unmarshal type DeliveryTimes");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DeliveryTimes)untypedValue;
            switch (value)
            {
                case DeliveryTimes.The024Часа:
                    serializer.Serialize(writer, "0-24 часа");
                    return;
                case DeliveryTimes.The13Дня:
                    serializer.Serialize(writer, "1-3 дня");
                    return;
                case DeliveryTimes.The1014Дней:
                    serializer.Serialize(writer, "10-14 дней");
                    return;
                case DeliveryTimes.The49Дней:
                    serializer.Serialize(writer, "4-9 дней");
                    return;
            }
            throw new Exception("Cannot marshal type DeliveryTimes");
        }

        public static readonly DeliveryTimesConverter Singleton = new DeliveryTimesConverter();
    }

    internal class InCheckoutConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(InCheckout) || t == typeof(InCheckout?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Null:
                    return new InCheckout { };
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new InCheckout { Bool = boolValue };
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    long l;
                    if (Int64.TryParse(stringValue, out l))
                    {
                        return new InCheckout { Integer = l };
                    }
                    break;
            }
            throw new Exception("Cannot unmarshal type InCheckout");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (InCheckout)untypedValue;
            if (value.IsNull)
            {
                serializer.Serialize(writer, null);
                return;
            }
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.Integer != null)
            {
                serializer.Serialize(writer, value.Integer.Value.ToString());
                return;
            }
            throw new Exception("Cannot marshal type InCheckout");
        }

        public static readonly InCheckoutConverter Singleton = new InCheckoutConverter();
    }

    internal class ServiceNameConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceName) || t == typeof(ServiceName?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return ServiceName.Empty;
                case "Деливери":
                    return ServiceName.Деливери;
                case "Зручна доставка":
                    return ServiceName.ЗручнаДоставка;
                case "Мист Экспресс":
                    return ServiceName.МистЭкспресс;
                case "Новая почта":
                    return ServiceName.НоваяПочта;
                case "Укрпочта":
                    return ServiceName.Укрпочта;
            }
            throw new Exception("Cannot unmarshal type ServiceName");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceName)untypedValue;
            switch (value)
            {
                case ServiceName.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case ServiceName.Деливери:
                    serializer.Serialize(writer, "Деливери");
                    return;
                case ServiceName.ЗручнаДоставка:
                    serializer.Serialize(writer, "Зручна доставка");
                    return;
                case ServiceName.МистЭкспресс:
                    serializer.Serialize(writer, "Мист Экспресс");
                    return;
                case ServiceName.НоваяПочта:
                    serializer.Serialize(writer, "Новая почта");
                    return;
                case ServiceName.Укрпочта:
                    serializer.Serialize(writer, "Укрпочта");
                    return;
            }
            throw new Exception("Cannot marshal type ServiceName");
        }

        public static readonly ServiceNameConverter Singleton = new ServiceNameConverter();
    }

    internal class ServiceNameAblativeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceNameAblative) || t == typeof(ServiceNameAblative?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Деливери":
                    return ServiceNameAblative.Деливери;
                case "Зручна доставка":
                    return ServiceNameAblative.ЗручнаДоставка;
                case "Мист Экспресс":
                    return ServiceNameAblative.МистЭкспресс;
                case "Новой почтой":
                    return ServiceNameAblative.НовойПочтой;
                case "Укрпочтой":
                    return ServiceNameAblative.Укрпочтой;
                case "курьером":
                    return ServiceNameAblative.Курьером;
            }
            throw new Exception("Cannot unmarshal type ServiceNameAblative");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceNameAblative)untypedValue;
            switch (value)
            {
                case ServiceNameAblative.Деливери:
                    serializer.Serialize(writer, "Деливери");
                    return;
                case ServiceNameAblative.ЗручнаДоставка:
                    serializer.Serialize(writer, "Зручна доставка");
                    return;
                case ServiceNameAblative.МистЭкспресс:
                    serializer.Serialize(writer, "Мист Экспресс");
                    return;
                case ServiceNameAblative.НовойПочтой:
                    serializer.Serialize(writer, "Новой почтой");
                    return;
                case ServiceNameAblative.Укрпочтой:
                    serializer.Serialize(writer, "Укрпочтой");
                    return;
                case ServiceNameAblative.Курьером:
                    serializer.Serialize(writer, "курьером");
                    return;
            }
            throw new Exception("Cannot marshal type ServiceNameAblative");
        }

        public static readonly ServiceNameAblativeConverter Singleton = new ServiceNameAblativeConverter();
    }

    internal class ServiceNameGenitiveConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceNameGenitive) || t == typeof(ServiceNameGenitive?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return ServiceNameGenitive.Empty;
                case "Деливери":
                    return ServiceNameGenitive.Деливери;
                case "Зручна доставка":
                    return ServiceNameGenitive.ЗручнаДоставка;
                case "Мист Экспресс":
                    return ServiceNameGenitive.МистЭкспресс;
                case "Новой почты":
                    return ServiceNameGenitive.НовойПочты;
                case "Укрпочты":
                    return ServiceNameGenitive.Укрпочты;
            }
            throw new Exception("Cannot unmarshal type ServiceNameGenitive");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceNameGenitive)untypedValue;
            switch (value)
            {
                case ServiceNameGenitive.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case ServiceNameGenitive.Деливери:
                    serializer.Serialize(writer, "Деливери");
                    return;
                case ServiceNameGenitive.ЗручнаДоставка:
                    serializer.Serialize(writer, "Зручна доставка");
                    return;
                case ServiceNameGenitive.МистЭкспресс:
                    serializer.Serialize(writer, "Мист Экспресс");
                    return;
                case ServiceNameGenitive.НовойПочты:
                    serializer.Serialize(writer, "Новой почты");
                    return;
                case ServiceNameGenitive.Укрпочты:
                    serializer.Serialize(writer, "Укрпочты");
                    return;
            }
            throw new Exception("Cannot marshal type ServiceNameGenitive");
        }

        public static readonly ServiceNameGenitiveConverter Singleton = new ServiceNameGenitiveConverter();
    }

    internal class AddressTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(AddressType) || t == typeof(AddressType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "address":
                    return AddressType.Address;
                case "pickup":
                    return AddressType.Pickup;
                case "warehouse":
                    return AddressType.Warehouse;
            }
            throw new Exception("Cannot unmarshal type AddressType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (AddressType)untypedValue;
            switch (value)
            {
                case AddressType.Address:
                    serializer.Serialize(writer, "address");
                    return;
                case AddressType.Pickup:
                    serializer.Serialize(writer, "pickup");
                    return;
                case AddressType.Warehouse:
                    serializer.Serialize(writer, "warehouse");
                    return;
            }
            throw new Exception("Cannot marshal type AddressType");
        }

        public static readonly AddressTypeConverter Singleton = new AddressTypeConverter();
    }

    internal class WarehouseTitleConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(WarehouseTitle) || t == typeof(WarehouseTitle?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "До склада")
            {
                return WarehouseTitle.ДоСклада;
            }
            throw new Exception("Cannot unmarshal type WarehouseTitle");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (WarehouseTitle)untypedValue;
            if (value == WarehouseTitle.ДоСклада)
            {
                serializer.Serialize(writer, "До склада");
                return;
            }
            throw new Exception("Cannot marshal type WarehouseTitle");
        }

        public static readonly WarehouseTitleConverter Singleton = new WarehouseTitleConverter();
    }

    internal class SameRegionCityUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SameRegionCityUnion) || t == typeof(SameRegionCityUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new SameRegionCityUnion { Bool = boolValue };
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    if (stringValue == "Киева")
                    {
                        return new SameRegionCityUnion { Enum = SameRegionCityEnum.Киева };
                    }
                    break;
            }
            throw new Exception("Cannot unmarshal type SameRegionCityUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (SameRegionCityUnion)untypedValue;
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.Enum != null)
            {
                if (value.Enum == SameRegionCityEnum.Киева)
                {
                    serializer.Serialize(writer, "Киева");
                    return;
                }
            }
            throw new Exception("Cannot marshal type SameRegionCityUnion");
        }

        public static readonly SameRegionCityUnionConverter Singleton = new SameRegionCityUnionConverter();
    }

    internal class SameRegionCityEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SameRegionCityEnum) || t == typeof(SameRegionCityEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "Киева")
            {
                return SameRegionCityEnum.Киева;
            }
            throw new Exception("Cannot unmarshal type SameRegionCityEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SameRegionCityEnum)untypedValue;
            if (value == SameRegionCityEnum.Киева)
            {
                serializer.Serialize(writer, "Киева");
                return;
            }
            throw new Exception("Cannot marshal type SameRegionCityEnum");
        }

        public static readonly SameRegionCityEnumConverter Singleton = new SameRegionCityEnumConverter();
    }

    internal class DeliverySettingsConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DeliverySettings) || t == typeof(DeliverySettings?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new DeliverySettings { Bool = boolValue };
                case JsonToken.StartObject:
                    var objectValue = serializer.Deserialize<Dictionary<string, DeliverySetting>>(reader);
                    return new DeliverySettings { DeliverySettingMap = objectValue };
                case JsonToken.StartArray:
                    var arrayValue = serializer.Deserialize<object[]>(reader);
                    return new DeliverySettings { AnythingArray = arrayValue };
            }
            throw new Exception("Cannot unmarshal type DeliverySettings");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (DeliverySettings)untypedValue;
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.AnythingArray != null)
            {
                serializer.Serialize(writer, value.AnythingArray);
                return;
            }
            if (value.DeliverySettingMap != null)
            {
                serializer.Serialize(writer, value.DeliverySettingMap);
                return;
            }
            throw new Exception("Cannot marshal type DeliverySettings");
        }

        public static readonly DeliverySettingsConverter Singleton = new DeliverySettingsConverter();
    }

    internal class FirmShopPointsUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(FirmShopPointsUnion) || t == typeof(FirmShopPointsUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new FirmShopPointsUnion { Bool = boolValue };
                case JsonToken.StartObject:
                    var objectValue = serializer.Deserialize<FirmShopPointsClass>(reader);
                    return new FirmShopPointsUnion { FirmShopPointsClass = objectValue };
            }
            throw new Exception("Cannot unmarshal type FirmShopPointsUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (FirmShopPointsUnion)untypedValue;
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.FirmShopPointsClass != null)
            {
                serializer.Serialize(writer, value.FirmShopPointsClass);
                return;
            }
            throw new Exception("Cannot marshal type FirmShopPointsUnion");
        }

        public static readonly FirmShopPointsUnionConverter Singleton = new FirmShopPointsUnionConverter();
    }

    internal class PointIdConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PointId) || t == typeof(PointId?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Integer:
                    var integerValue = serializer.Deserialize<long>(reader);
                    return new PointId { Integer = integerValue };
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    return new PointId { String = stringValue };
            }
            throw new Exception("Cannot unmarshal type PointId");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (PointId)untypedValue;
            if (value.Integer != null)
            {
                serializer.Serialize(writer, value.Integer.Value);
                return;
            }
            if (value.String != null)
            {
                serializer.Serialize(writer, value.String);
                return;
            }
            throw new Exception("Cannot marshal type PointId");
        }

        public static readonly PointIdConverter Singleton = new PointIdConverter();
    }

    internal class GuaranteeTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(GuaranteeType) || t == typeof(GuaranteeType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "От магазина":
                    return GuaranteeType.ОтМагазина;
                case "От производителя":
                    return GuaranteeType.ОтПроизводителя;
            }
            throw new Exception("Cannot unmarshal type GuaranteeType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (GuaranteeType)untypedValue;
            switch (value)
            {
                case GuaranteeType.ОтМагазина:
                    serializer.Serialize(writer, "От магазина");
                    return;
                case GuaranteeType.ОтПроизводителя:
                    serializer.Serialize(writer, "От производителя");
                    return;
            }
            throw new Exception("Cannot marshal type GuaranteeType");
        }

        public static readonly GuaranteeTypeConverter Singleton = new GuaranteeTypeConverter();
    }

    internal class MerchantTitleConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(MerchantTitle) || t == typeof(MerchantTitle?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return MerchantTitle.Empty;
                case "Cherries":
                    return MerchantTitle.Cherries;
                case "DMS":
                    return MerchantTitle.Dms;
            }
            throw new Exception("Cannot unmarshal type MerchantTitle");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (MerchantTitle)untypedValue;
            switch (value)
            {
                case MerchantTitle.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case MerchantTitle.Cherries:
                    serializer.Serialize(writer, "Cherries");
                    return;
                case MerchantTitle.Dms:
                    serializer.Serialize(writer, "DMS");
                    return;
            }
            throw new Exception("Cannot marshal type MerchantTitle");
        }

        public static readonly MerchantTitleConverter Singleton = new MerchantTitleConverter();
    }

    internal class PriceLineTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PriceLineType) || t == typeof(PriceLineType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "tx")
            {
                return PriceLineType.Tx;
            }
            throw new Exception("Cannot unmarshal type PriceLineType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (PriceLineType)untypedValue;
            if (value == PriceLineType.Tx)
            {
                serializer.Serialize(writer, "tx");
                return;
            }
            throw new Exception("Cannot marshal type PriceLineType");
        }

        public static readonly PriceLineTypeConverter Singleton = new PriceLineTypeConverter();
    }

    internal class PriceOldConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PriceOld) || t == typeof(PriceOld?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new PriceOld { Bool = boolValue };
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    return new PriceOld { String = stringValue };
            }
            throw new Exception("Cannot unmarshal type PriceOld");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (PriceOld)untypedValue;
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.String != null)
            {
                serializer.Serialize(writer, value.String);
                return;
            }
            throw new Exception("Cannot marshal type PriceOld");
        }

        public static readonly PriceOldConverter Singleton = new PriceOldConverter();
    }

    internal class PriceUsdRealUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PriceUsdRealUnion) || t == typeof(PriceUsdRealUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.String:
                case JsonToken.Date:
                    var stringValue = serializer.Deserialize<string>(reader);
                    if (stringValue == "9&nbsp;999&nbsp;999")
                    {
                        return new PriceUsdRealUnion { Enum = PriceUsdRealEnum.The9Nbsp999Nbsp999 };
                    }
                    long l;
                    if (Int64.TryParse(stringValue, out l))
                    {
                        return new PriceUsdRealUnion { Integer = l };
                    }
                    break;
            }
            throw new Exception("Cannot unmarshal type PriceUsdRealUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (PriceUsdRealUnion)untypedValue;
            if (value.Enum != null)
            {
                if (value.Enum == PriceUsdRealEnum.The9Nbsp999Nbsp999)
                {
                    serializer.Serialize(writer, "9&nbsp;999&nbsp;999");
                    return;
                }
            }
            if (value.Integer != null)
            {
                serializer.Serialize(writer, value.Integer.Value.ToString());
                return;
            }
            throw new Exception("Cannot marshal type PriceUsdRealUnion");
        }

        public static readonly PriceUsdRealUnionConverter Singleton = new PriceUsdRealUnionConverter();
    }

    internal class PriceUsdRealEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PriceUsdRealEnum) || t == typeof(PriceUsdRealEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "9&nbsp;999&nbsp;999")
            {
                return PriceUsdRealEnum.The9Nbsp999Nbsp999;
            }
            throw new Exception("Cannot unmarshal type PriceUsdRealEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (PriceUsdRealEnum)untypedValue;
            if (value == PriceUsdRealEnum.The9Nbsp999Nbsp999)
            {
                serializer.Serialize(writer, "9&nbsp;999&nbsp;999");
                return;
            }
            throw new Exception("Cannot marshal type PriceUsdRealEnum");
        }

        public static readonly PriceUsdRealEnumConverter Singleton = new PriceUsdRealEnumConverter();
    }

    internal class SaleUnionConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SaleUnion) || t == typeof(SaleUnion?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Boolean:
                    var boolValue = serializer.Deserialize<bool>(reader);
                    return new SaleUnion { Bool = boolValue };
                case JsonToken.StartObject:
                    var objectValue = serializer.Deserialize<SaleClass>(reader);
                    return new SaleUnion { SaleClass = objectValue };
            }
            throw new Exception("Cannot unmarshal type SaleUnion");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (SaleUnion)untypedValue;
            if (value.Bool != null)
            {
                serializer.Serialize(writer, value.Bool.Value);
                return;
            }
            if (value.SaleClass != null)
            {
                serializer.Serialize(writer, value.SaleClass);
                return;
            }
            throw new Exception("Cannot marshal type SaleUnion");
        }

        public static readonly SaleUnionConverter Singleton = new SaleUnionConverter();
    }

    internal class ReviewsQuantityTitleConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ReviewsQuantityTitle) || t == typeof(ReviewsQuantityTitle?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "отзыв":
                    return ReviewsQuantityTitle.Отзыв;
                case "отзыва":
                    return ReviewsQuantityTitle.Отзыва;
                case "отзывов":
                    return ReviewsQuantityTitle.Отзывов;
            }
            throw new Exception("Cannot unmarshal type ReviewsQuantityTitle");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ReviewsQuantityTitle)untypedValue;
            switch (value)
            {
                case ReviewsQuantityTitle.Отзыв:
                    serializer.Serialize(writer, "отзыв");
                    return;
                case ReviewsQuantityTitle.Отзыва:
                    serializer.Serialize(writer, "отзыва");
                    return;
                case ReviewsQuantityTitle.Отзывов:
                    serializer.Serialize(writer, "отзывов");
                    return;
            }
            throw new Exception("Cannot marshal type ReviewsQuantityTitle");
        }

        public static readonly ReviewsQuantityTitleConverter Singleton = new ReviewsQuantityTitleConverter();
    }

    internal class StockTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(StockType) || t == typeof(StockType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "":
                    return StockType.Empty;
                case "в наличии":
                    return StockType.ВНаличии;
                case "под заказ":
                    return StockType.ПодЗаказ;
            }
            throw new Exception("Cannot unmarshal type StockType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (StockType)untypedValue;
            switch (value)
            {
                case StockType.Empty:
                    serializer.Serialize(writer, "");
                    return;
                case StockType.ВНаличии:
                    serializer.Serialize(writer, "в наличии");
                    return;
                case StockType.ПодЗаказ:
                    serializer.Serialize(writer, "под заказ");
                    return;
            }
            throw new Exception("Cannot marshal type StockType");
        }

        public static readonly StockTypeConverter Singleton = new StockTypeConverter();
    }
}
