﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using HtmlAgilityPack;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hotline.Grabber
{
    class Program
    {
        static void Main(string[] args)
        {

            var readCells =  "F3:F19";

            try
            {
                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                var filePath = @"C:\Users\User\Downloads\Mobalex price .xlsx";
                var hasFile = File.Exists(filePath);
                Console.WriteLine(hasFile);
                excelApp.Workbooks.Open(filePath, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing);
                var ws = excelApp.Worksheets;
                var worksheet = (Worksheet) ws.get_Item("Лист1");
                var range = worksheet.UsedRange;
                object[,] values = (object[,]) range.Value2;

                var cells = readCells.Split(':');

                var readColIndex = (cells[0][0] - 'A' + 1);
                var rowStart = int.Parse(cells[0].Substring(1));
                var rowEnd = int.Parse(cells[1].Substring(1));
                Console.WriteLine(rowStart +" - -" +rowEnd);
                for (int row = rowStart; row <= rowEnd; row++)
                {

                    string url = values[row, readColIndex]?.ToString();
                    Console.WriteLine();
                    
                    Console.WriteLine($"read row [{row},{readColIndex}] => {url} ");

                    if ((url?.StartsWith("https://hotline.ua/") ?? false) || (url?.StartsWith("http://hotline.ua/") ?? false))
                    {
                        var segments = url.Split('/');
                        url = $"http://hotline.ua/{segments[3]}/{segments[4]}";
                        

                        var token = GetToken(url);
                        Console.WriteLine($"-- {url} -- {token}");
                        var prices = GetPrices(url, token);

                        Console.WriteLine($"price: {prices}");
                        range.Cells.set_Item(row, readColIndex + 1, prices);
                    }
                    Console.WriteLine("---End---");
                }

                excelApp.Save(filePath);
                excelApp.Quit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //}
            }

        }

        private static string GetPrices(string url, string token)
        {
            using (var http = new HttpClient())
            {

                http.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
                http.DefaultRequestHeaders.Add("referer", url);
                http.DefaultRequestHeaders.Add("x-csrf-token", token);
                
                var response = http.GetAsync(url.TrimEnd('/') + "/load-prices/").Result;
                Console.WriteLine(response.StatusCode);

                string respStr = null;
                try
                {
                    using (var stream = response.Content.ReadAsStreamAsync().Result)
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        respStr = sr.ReadToEnd();
                        File.WriteAllText("price_raw.json", respStr.ToString());
                        //Console.WriteLine(json.prices);
                        var json = JsonConvert.DeserializeObject<PriceModel>(respStr);
                        int i = 0;

                        var price = json.Prices.Where(p => p.FirmCityId == 187).Min(p => p.PriceUah).ToString("D5");
                        foreach (var p in json.Prices.Where(p => p.FirmCityId == 187))
                        {
                            Console.WriteLine(++i + " " + p.FirmCityId + " " + p.PriceUah);
                        }

                        return price;
                        //Console.WriteLine(json);
                        //do your status code logic here
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine(respStr);
                }
                

                return "error";
            }
        }

        public static string GetToken(string url)
        {
            using (var http = new HttpClient())
            {

                http.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36");
                http.DefaultRequestHeaders.Add("referer", "https://hotline.ua/mobile/");
                var response = http.GetAsync(url).Result;
                var body = response.Content.ReadAsStringAsync().Result;
                File.WriteAllText("index.html", body);

                HtmlDocument htmlSnippet = new HtmlDocument();
                htmlSnippet.LoadHtml(body);

                List<string> hrefTags = new List<string>();

                foreach (HtmlNode link in htmlSnippet.DocumentNode.SelectNodes("//meta"))
                {
                    var name = link.Attributes.FirstOrDefault(a => a.Name == "name");
                    if (name?.Value == "csrf-token")
                    {
                        var token = link.Attributes.FirstOrDefault(a => a.Name == "content");
                        //Console.WriteLine(token.Value);
                        return token.Value;
                    }

                }
                return null;
            }
        }
    }
}
